import 'package:flutter/material.dart';
import 'package:flutter_provider_example/provider/providers/counter.dart';
import 'package:provider/provider.dart';

class Count extends StatelessWidget {
  const Count({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      /// Calls `context.watch` to make [Count] rebuild when [Counter] changes.
      '${context.watch<Counter>().count}',
      
      style: Theme.of(context).textTheme.headline4,
    );
  }
}
