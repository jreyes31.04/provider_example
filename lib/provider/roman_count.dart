import 'package:flutter/cupertino.dart';
import 'package:flutter_provider_example/provider/providers/roman_counter.dart';
import 'package:provider/provider.dart';

class RomanCount extends StatelessWidget {
  const RomanCount({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(context.watch<RomanCounter>().romanCounter.toString());
  }
}
