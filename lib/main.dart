import 'package:flutter/material.dart';
import 'package:flutter_provider_example/provider/providers/counter.dart';
import 'package:flutter_provider_example/provider/home_page.dart';
import 'package:flutter_provider_example/provider/providers/roman_counter.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => Counter(),
        ),
        ChangeNotifierProvider(
          create: (_) => RomanCounter(),
        ),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const ProviderHomePage(),
    );
  }
}
